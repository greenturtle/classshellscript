#!/bin/bash
# Program: parameters2.sh
# Dependencies: none
# Author: Vinicius Richard Martiori (vrmsupport@protonmail.com)
# Description: Show the program name, squared the first parameter and show the total of parameters
# Parameters: anyNumber,  anyParameter
# Return: 0
# Example: chmod +x parameters2.sh;
nome=$0

echo "Program name: $nome"
if [ $1 ]; then
	squared=$[ $1**2 ]
	echo "$1 squared  is: $squared"
fi

# $#, total parameters

echo "Total parameters = $#"

#!/bin/bash
# Program: conditional.sh
# Dependencies: none
# Author: Vinicius Richard Martiori (vrmsupport@protonmail.com)
# Description: My first script using if and else
# Parameters: none
# Return: 0
# Example: chmod +x conditional.sh; ./conditional.sh

echo "Please enter your username: "
read NAME

if [ "$NAME" = "Elliot" ]; then
	echo "Wellcome back Elliot"
else
	echo "Invalid username, please register an account"
fi

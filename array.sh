#!/bin/bash
# Program: array.sh
# Dependencies: none
# Author: Vinicius Richard Martiori (vrmsupport@protonmail.com)
# Description: Testing array in shell script
# Parameters: none
# Return: 0
# Example: chmod +x array.sh; ./array.sh

testarray=(orange strawbarry grape banana pear)

echo ${testarray[*]}
echo ${testarray[0]}
unset testarray[*]
echo ${testarray[*]}

#!/bin/bash
# Program: test.sh
# Dependencies: none
# Author: Vinicius Richard Martiori (vrmsupport@protonmail.com)
# Description: Tests the shell script syntax and mathematical operations
# Parameters: none
# Return: 0
# Example: chmod +x test.sh; ./test.sh

#Comment

echo "Hello World!"

myName="Derek"
echo "My name is:$myName"

declare -r NUM1=5
num2=4

num3=$((NUM1+num2))
echo $num3

echo $((5**7))

rand=5
let rand+=4
echo "$rand"

echo "rand++ = $((rand++))"
echo "++rand = $((++rand))"

cat<< END
This text
prints on
many lines
END

#!/bin/bash
# Program: read3.sh
# Dependencies: none
# Author: Vinicius Richard Martiori (vrmsupport@protonmail.com)
# Description: Read examples with conditional case and read as an password
# Parameters: none
# Return: 0
# Example: chmod +x read3.sh; ./read3.sh

echo "Do you wanna continue?[S/n]"
read -n1 answer
case $answer in
	S|s)echo
		echo "Running....";;
	N|n)echo
		echo "Aborted...";;
	*)echo
		echo "Invalid option!";;
esac

echo "Reading as a password, type something: "
read -s passwd
echo "You typed: $passwd"

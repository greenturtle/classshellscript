#!/bin/bash
# Program: flags.sh
# Dependencies: none
# Author: Vinicius Richard Martiori (vrmsupport@protonmail.com)
# Description: Allow put flags in any particular order
# Parameters: none
# Return: 0
# Example: chmod +x flags.sh; ./flags.sh

while getopts x:y: aflag
do
	case $aflag in
		x)echo "The x is: $OPTARG";;
		y)echo "The y is: $OPTARG";;
		?)echo "Invalid flag: $OPTARG";;
	esac
done

#!/bin/bash
# Program: case.sh
# Dependencies: none
# Author: Vinicius Richard Martiori (vrmsupport@protonmail.com)
# Description: It's a test script using case
# Parameters: none
# Return: 0
# Example: chmod +x case.sh; ./case

room="liveroom"

case $room in
	bathroom)
		echo "Take a bath";;
	kitchen)
		echo "Coock the chicken";;
	liveroom)
		echo "Watch TV";;
	backyard | yard)
		echo "Bring me some flowers, please";;
	*)
		echo "You are not in the house";;
esac

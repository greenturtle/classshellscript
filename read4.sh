#!/bin/bash
# Program: read4.sh
# Dependencies: The file 'arq1.txt' in the same directory
# Author: Vinicius Richard Martiori (vrmsupport@protonmail.com)
# Description: Read data from a file, in this case a name list
# Parameters: none
# Return: 0
# Example: chmod +x read4.sh; ./read4.sh

cont=1
cat arq1.txt | while read line
do
	echo "Line $cont: $line"
	cont=$[ $cont + 1 ]
done

#!/bin/bash
# Program: if.sh
# Dependencies: none
# Author: Vinicius Richard Martiori (vrmsupport@protonmail.com)
# Description: If examples
# Parameters: none
# Return: 0
# Example: chmod +x if.sh; ./if.sh
site="www.google.com"
 if wget -q --spider "$site"; then
	echo "Connected"
else
	echo "Disconnected"
 fi

if ((1>0));then
	echo "1 is great then 0"
fi
#if check the last command is valid
# = 0 ; Sucess
#!= 0 ; Error

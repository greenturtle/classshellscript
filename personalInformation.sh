#!/bin/bash
# Program: personalInformation.sh
# Dependencies: none
# Author: Vinicius Richard Martiori (vrmsupport@protonmail.com)
# Description: This program asks for your personal information
# Parameters: none
# Return: 0
# Example: chmod +x personalInformation.sh; ./personalInformation.sh

echo "fill the form: "
for questions in Name Age Cellphone
do
	read -p "${questions[@]} : " ${questions}
done

echo -e "Your data\nName: $Name\nAge: $Age\nCellphone: $Cellphone"

#!/bin/bash
# Program: until.sh
# Dependencies: none
# Author: Vinicius Richard Martiori (vrmsupport@protonmail.com)
# Description: Until example
# Parameters: none
# Return: 0
# Example: chmod +x until.sh; ./until.sh

var=50

until [ $var -eq 0 ]
do
	echo $var
	var=$[ $var - 2 ]
done

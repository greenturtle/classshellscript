#!/bin/bash
# Program: elevator.sh
# Dependencies: none
# Author: Vinicius Richard Martiori (vrmsupport@protonmail.com)
# Description: Infinite loop elevator
# Parameters: none
# Return: 0
# Example: chmod +x elevator.sh; ./elevator
# Press enter to go faster

function test {
	echo $1
}

function cable {
repeat=$1
count=0
until [ $count -eq $repeat  ]
do
cat<< END
             ||
END
	count=$[ $count + 1 ]
done
}


function elevator(){
cat<< END
            #||#
  #---------------------#
  |                     |
  |                     |
  |                     |
  |                     |
  |_____________________|
  |                     |
  |                     |
  |                     |
  |_____________________|
END
}
#SCRIPT
for ((i=0; i<=19; i++))
do
	if [ $i -eq 19 ]; then
		until [ $i -eq 0 ]
		do
			clear
			cable $i
			elevator
			read -t 1
			i=$[ $i - 1 ]
		done
	fi
	clear
	cable $i
	elevator
	read -t 1
done

#!/bin/bash
# Program: shift.sh
# Dependencies: none
# Author: Vinicius Richard Martiori (vrmsupport@protonmail.com)
# Description: Shift command example
# Parameters: Whatever you want
# Return: 0
# Example: chmod +x shift.sh; ./shift.sh 1 2 3 4 5 6 7

i=1
while [ -n "$1" ]
do
	echo "The param 1 has the value = $1"
	i=$[ $i + 1 ]
	shift
done

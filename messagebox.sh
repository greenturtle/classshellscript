#!/bin/bash
# Program: messagebox.sh
# Dependencies: none
# Author: Vinicius Richard Martiori (vrmsupport@protonmail.com)
# Description: A simple messagebox
# Parameters: none
# Return: 0
# Example: chmod + messagebox.sh; ./messagebox
whiptail --title "MessageBox" --msgbox "Messabox with  whiptail.Press OK to continue ." --fb 10 50

#!/bin/bash
# Program: for.sh
# Dependencies: none
# Author: Vinicius Richard Martiori (vrmsupport@protonmail.com)
# Description: For examples
# Parameters: none
# Return: 0
# Example: chmod +x for.sh; ./for.sh

for food in cookie cake massa 'white chocolate'
do
	echo "I love: $food"
done

#IFS, internal field separator, default: espace, tab and newline.

IFS=$'\n'

arq1=arq1.txt
for name in `cat $arq1`
do
	echo "name: $name"
done

IFS=$IFSOLD

for (( i=0; i<=10; i++))
do
	echo $i
done

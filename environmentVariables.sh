#!/bin/bash
# Program: environmentVaribales.sh
# Dependencies: none
# Author: Vinicius Richard Martiori (vrmsupport@protonmail.com)
# Description: A simple script to learn about environment variables
# Parameters: none
# Return: 0
# Example: chmod +x environmentVaribales; ./environmentVaribales.sh

my_variable="Hello World"
echo $my_variable

my_variable="Null"
echo $my_variable

my_variable="I am Global now!"
export my_variable
echo $my_variable

unset my_variable
echo $my_variable

PATH=$PATH:/home/artemis/Class/ShellScripting

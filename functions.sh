#!/bin/bash
# Program: functions.sh
# Dependencies: none
# Author: Vinicius Richard Martiori (vrmsupport@protonmail.com)
# Description: Function Examples
# Parameters: none
# Return: 0
# Example: chmod +x functions.sh; ./functions.sh
getDate(){
	date
	return
}

getDate

name="Derek"

demLocal(){
	local name="Paul"
	return
}

demLocal
echo "$name"

getSum(){
	local num3=$1
	local num4=$2

	local sum=$((num3+num4))
	echo $sum

}

num1=5
num2=6

sum=$(getSum num1 num2)
echo "The sum is $sum"

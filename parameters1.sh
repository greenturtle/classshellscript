#!/bin/bash
# Program: parameters1.sh
# Dependencies: none
# Author: Vinicius Richard Martiori (vrmsupport@protonmail.com)
# Description: A simple parameter example
# Parameters: -a, -b, -c
# Return: 0
# Example: chmod +x parameters1.sh; ./parameters1.sh -a

while [ -n "$1" ]
do
	case "$1" in
	-a)echo "You selected: A";;
	-b)echo "You selected: B";;
	-c)echo "You selected: C";;
	*)echo "The '$1' parameter is INVALID!";;
	esac
	shift
done

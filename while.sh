#!/bin/bash
# Program: while.sh
# Dependencies: none
# Author: Vinicius Richard Martiori (vrmsupport@protonmail.com)
# Description: While example
# Parameters: none
# Return: 0
# Example: chmod +x while; ./while.sh

#while 0
x=0
while [ $x -eq 0 ]
do
	echo "type: yes"
	read y
	if [ "$y" = "yes" ]; then
		x=1
		echo "Bye..."
	fi
done

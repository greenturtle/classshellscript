#!/bin/bash
# Program: functions2.sh
# Dependencies: none
# Author: Vinicius Richard Martiori (vrmsupport@protonmail.com)
# Description: Function Examples
# Parameters: none
# Return: 0
# Example: chmod +x functions2.sh; ./functions2.sh
function func_1 {
	echo "Hi 1"
}

func_2(){
	echo "Hi 2"
}

func_1
func_2

function squared {
	read -p "Type a number: " number
	echo $[ $number**2 ]
}

result=`squared`

echo "Result: $result"

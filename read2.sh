#!/bin/bash
# Program: read2.sh
# Dependencies: none
# Author: Vinicius Richard Martiori (vrmsupport@protonmail.com)
# Description: Read examples
# Parameters: none
# Return: 0
# Example: chmod +x read2.sh

read -p "How old are you? "
echo "You are $REPLY years old"

#timeout
echo "What's your name? "

if read -t 5 name
then
	echo "Wellcome $name"
else
	echo "It's too late!"
fi

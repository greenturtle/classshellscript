#!/bin/bash
# Program: permission.sh
# Dependencies: none
# Author: Vinicius Richard Martiori (vrmsupport@protonmail.com)
# Description: A simple script to learn about user permissions
# Parameters: none
# Return: 0
# Example: chmod +x permission.sh; ./permission.sh

if [ true ]; then
	echo "true"
elif [ false ]; then
	echo "false"
fi

if [ $USER = artemis ];then
	echo "You have write permission to change the .bashrc file"
else
	echo "You dont have write permission to change the .bashrc file"
fi
